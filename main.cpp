#include <iostream>
#include <ctime>
#include <iomanip>
#include <sstream>

using namespace std;

const unsigned int fdos = 80;
const unsigned int fdom = 172;
const unsigned int fdof = 265;
const unsigned int fdow = 356;

enum class Season {
    WINTER, SPRING, SUMMER, AUTUMN
};

string SeasonName(Season s) {
    switch(s){
        case Season::WINTER: return "Winter";
        case Season::SPRING: return "Spring";
        case Season::SUMMER: return "Summer";
        case Season::AUTUMN: return "Autumn";
        default: return "Something is effed up";
    }
}

Season GetSeason(int doy) {
    if(doy >= 356 || doy <= 79) return Season::WINTER;
    else if(doy >= 80 && doy <= 171) return Season::SPRING;
    else if(doy >= 172 && doy <= 264) return Season::SUMMER;
    else return Season::AUTUMN;
}

string nFormat(unsigned int doy) {
    ostringstream os;
    auto hdoy = doy + 1;
    auto tens = hdoy % 100;
    os << hdoy;
    if(tens > 11  && tens < 14) os << "th";
    else {
        switch (hdoy % 10) {
            case 1: os << "st"; break;
            case 2: os << "nd"; break;
            case 3: os << "rd"; break;
            default: os << "th"; break;
        }
    }
    return os.str();
}

unsigned int GetDayOfSeason(unsigned int doy) {
    switch (GetSeason(doy)) {
        case Season::WINTER :
            if(doy >= 356) return doy - 355; else return doy + 9;
        case Season::SPRING: return doy - fdos;
        case Season::SUMMER: return doy - fdom;
        default: return doy - fdof;
    }
}

string GetPart(unsigned int doy) {
    auto dos = GetDayOfSeason(doy);
    if (dos >= 0 && dos <= 10) return "Early";
    if (dos > 10 && dos <= 20) return "mid Early";
    if (dos > 20 && dos <= 30) return "late Early";
    if (dos > 30 && dos <= 40) return "early mid";
    if (dos > 40 && dos <= 51) return "Mid";
    if (dos > 51 && dos <= 61) return "late Mid";
    if (dos > 61 && dos <= 71) return "early Late";
    if (dos > 71 && dos <= 81) return "mid Late";
    if (dos > 81 && dos <= 95) return "Late";
    return "";
}

unsigned int GetDaysUntil(unsigned int doy, unsigned int fdoy) {
    fdoy += doy >= fdoy ? 365 : 0;
    auto until = fdoy - doy;
    if(until >= 0) return until; else return 365 + until;
}


int main()
{
    time_t now = time(nullptr);
    tm lt = *localtime(&now);
    ostringstream os;
    os << put_time(&lt, "%c");
    unsigned int tdoy = lt.tm_yday;
    unsigned int sdoy = tdoy + 9;
    unsigned int csdoy = GetDayOfSeason(tdoy);
    char buff[48];


    Season season = GetSeason(tdoy);

    cout << setprecision(3);
    cout << "-----------------------------------------------------" << endl;

    cout << os.str() << endl;
    cout << "Today is the " << nFormat(tdoy) << " day of the calendar year\n";
    cout << "Today is the " << nFormat(sdoy) << " day of the seasonal year\n";
    sprintf(buff, "%3.1f", GetDayOfSeason(tdoy)/.9125);
    cout << "Today is the " << nFormat(csdoy) << " day of " << SeasonName(season) << " " << buff << "%\n";
    cout << "Its " << GetPart(tdoy) << " " << SeasonName(season) << endl << endl;
    if(season != Season::WINTER) cout << GetDaysUntil(tdoy, fdow) << " days until Winter\n";
    if(season != Season::SPRING) cout << GetDaysUntil(tdoy, fdos) << " days until Spring\n";
    if(season != Season::SUMMER) cout << GetDaysUntil(tdoy, fdom) << " days until Summer\n";
    auto duf = GetDaysUntil(tdoy, fdof);
    sprintf(buff, "%3.1f", (duf/365.0)*100.0);
    cout << duf << " days until Autumn (" << buff << "% left)\n";

    cout << "-----------------------------------------------------\n" << endl;
    return 0;
}


