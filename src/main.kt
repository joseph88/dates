
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*

var fdoy: Int = 1
const val fdos = 80
const val fdom = 172
const val fdof = 265
const val fdow = 356

fun main(args: Array<String>) {
    val format = SimpleDateFormat("EEEE MMMM d, yyyy")
    val cal = Calendar.getInstance()
    val tdoy = cal.get(DAY_OF_YEAR)
    val datestr = format.format(cal.time)

    //cal.set(2018, JANUARY, 1)
    //val ff = cal.get(DAY_OF_YEAR)
    //println("ff = $ff")

    val mdspring = ((fdom-fdos)/2) + fdos
    cal.set(DAY_OF_YEAR, mdspring)
    //println("Mid Spring: ${format.format(cal.time)}")

    val mdsmr = ((fdof-fdom)/2) + fdom
    cal.set(DAY_OF_YEAR, mdsmr)
    //println("Mid Summer: ${format.format(cal.time)}")

    val mdfall = ((fdow-fdof)/2) + fdof
    cal.set(DAY_OF_YEAR, mdfall)
    //println("Mid Fall: ${format.format(cal.time)}")

    val mdwntr = ((fdos-fdoy + 365-fdow)/2) - (365-fdow)
    cal.set(DAY_OF_YEAR, mdwntr)
    //println("Mid Winter: ${format.format(cal.time)}")

    //println("${52%10}")

    println("------------------------------------------------------")
    println(datestr)
    println("Today is the ${nFormat(tdoy)} day of the calendar year")
    println("Today is the ${nFormat(getDayOfSeasonalYear(tdoy))} day of the seasonal year")

    val season = getSeason(tdoy)
    println("Today is the ${nFormat(getDayOfSeason(tdoy))} day of $season (${"%.1f".format(getDayOfSeason(tdoy).toDouble()/.9125)}%)")
    println("Its ${getPart(tdoy)} $season")
    println()
    if(season != Season.WINTER) println("${getDaysUntil(tdoy, fdow)} days until Winter")
    if(season != Season.SPRING) println("${getDaysUntil(tdoy, fdos)} days until Spring")
    if(season != Season.SUMMER) println("${getDaysUntil(tdoy, fdom)} days until Summer")
    val duf = getDaysUntil(tdoy, fdof)
    println("${duf} days until Fall (${"%.1f".format((duf/365.0)*100)}% left)")
    println("------------------------------------------------------")

    //for (i in 180..185) println("$i = ${getPart(i)}")
}

enum class Season {
    WINTER, SPRING, SUMMER, AUTUMN;
    override fun toString() : String {
        return when (this.ordinal) {
            WINTER.ordinal -> "Winter"
            SPRING.ordinal -> "Spring"
            SUMMER.ordinal -> "Summer"
            else -> "Autumn"
        }
    }
}

fun getSeason(doy: Int) : Season {
    if(doy >= 356 || doy < 80) return Season.WINTER
    else if(doy in 80..171) return Season.SPRING
    else if(doy in 172..264) return Season.SUMMER
    else return Season.AUTUMN
}

fun getDayOfSeasonalYear(doy: Int) : Int = doy + 9

fun getDayOfSeason(doy: Int) : Int {
    return when (getSeason(doy)) {
        Season.WINTER -> {
            if(doy >= 356) doy-355 else doy + 9
        }
        Season.SPRING -> doy - fdos
        Season.SUMMER -> doy - fdom
        else -> doy - fdof
    }
}

fun getDaysUntil(doy: Int, fdoy: Int) : Int{
    val until = fdoy - doy
    return if(until >= 0) until
    else 365 + until
}

fun nFormat(doy: Int) : String{
    val hdoy = doy+1
    val tens = hdoy%100
    val suff = if(tens in 11..14) {
        "th"
    }else {
        when (hdoy % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }
    return "$hdoy$suff"
}

fun getPart(doy: Int) : String {
    return when(getDayOfSeason(doy)){
        in 0..10 -> "Early"
        in 10..20 -> "mid Early"
        in 20..30 -> "late Early"
        in 30..40 -> "early Mid"
        in 40..50 -> "Mid"
        in 50..60 -> "late Mid"
        in 60..70 -> "early Late"
        in 70..80 -> "mid Late"
        in 80..95 -> "Late"
        else -> ""
    }
}